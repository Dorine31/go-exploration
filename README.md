# Explore GOLANG
source : https://www.youtube.com/@getCodingKnowledge

1. Download Go
2. Add Go extension to vs code

## Run the program
```
go run ./main.go
```
## create an executable
```
go build ./main.go
```
## create a module
```
go mod init PathToModule/UrlToModule
```
## Run tests
```
go mod test
```
## Run the executable
```
./main.exe
```