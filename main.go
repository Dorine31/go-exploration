package main

import (
	"fmt"
)

func first[T int32 | float32](a, b T) T {
	return a
}
// add ~ to accept float64 types OR subtypes
func second[T ~float64](a, b T) T {
	return b
}

type Number interface {
	int8 | int16 | int32 | int64 | int | uint | uint8 | uint16 |uint32 |uint64 |float32 | ~float64
}

// type set
func min[T Number](x, y T) T {
	if x < y {
		return x
	} else {
		return y
	}
}

func main() {
	// type parameters
	fmt.Println(first[int32](5, 8))
	fmt.Println(first[float32](5.2, 8.2))

	// type inference
	fmt.Println(second(5.2, 8.2))
	type f float64
	var foo f = 3.14
	fmt.Println(second(foo, 8.2))

	fmt.Println(min(2, 5))
}